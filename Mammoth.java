public class Mammoth{
	private String diet;
	private String type;
	private int height;
	
	public Mammoth(String diet, String type){
		this.diet = diet;
		this.type =  type;
		this.height =  1;
	}
	
	public String getDiet(){
		return this.diet;
	}
	
	public String getType(){
		return this.type;
	}
	
	public void setHeight(int height){
		this.height = height;
	}
	public int getHeight(){
		return this.height;
	}
		
	public void eatFood(){
		if(this.diet.equalsIgnoreCase("Plants")){
			System.out.println("Yum I am eating plants");
		}
		else if(this.diet.equalsIgnoreCase("Forbs")){
			System.out.println("Yum I am eating forbs");
		}
		else{
			System.out.println("I am eating grasses");
		}
	}
	public void rankMammoth(){
		if(this.height > 4){
			System.out.println("Yum I am one of the tallest types of mammoths");
		}
		else if(this.height == 1){
			System.out.println("I am the smallest mammoth type");
		}
		else{
			System.out.println("I am a medium sized mammoth");
		}
	}
}
