import java.util.Scanner;

public class VirtualPetApp{

	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Mammoth [] herdmammoths = new Mammoth [1];
		
		for(int i = 0; i < herdmammoths.length; i++){
			System.out.println("Enter the diet of the mammoth: ");
			String givenDiet = reader.nextLine();
			System.out.println("Enter the type of the mammoth: ");
			String givenType = reader.nextLine();
		
			herdmammoths[i] = new Mammoth(givenDiet, givenType);
			
			herdmammoths[i].getType();
			herdmammoths[i].getDiet();

			System.out.println("Enter the height of the mammoth: ");
			int givenHeight = Integer.parseInt(reader.nextLine());
			herdmammoths[i].setHeight(givenHeight);
			herdmammoths[i].getHeight(); 
		}
	
	System.out.println("Current result of last mammoth:");
	System.out.println(herdmammoths[herdmammoths.length - 1].getType());
	System.out.println(herdmammoths[herdmammoths.length - 1].getDiet());
	System.out.println(herdmammoths[herdmammoths.length - 1].getHeight());
	
	System.out.println("Enter the height of the last mammoth again: ");
	int givenHeight = Integer.parseInt(reader.nextLine());
	herdmammoths[herdmammoths.length - 1].setHeight(givenHeight);
		
	System.out.println("New results:");
	System.out.println(herdmammoths[herdmammoths.length - 1].getType());
	System.out.println(herdmammoths[herdmammoths.length - 1].getDiet());
	System.out.println(herdmammoths[herdmammoths.length - 1].getHeight());

	}
}
